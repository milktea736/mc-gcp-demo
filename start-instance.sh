#!/bin/bash

# you should edit a  machine name for yourself
VM_NAME='mc-command'

# default settings
PROJECT='dataecolab-lab'
ZONE='asia-east1-a'
SOURCE_MACHINE_IMAGE_NAME='mc-base-image'

BASEDIR=$(cd "$(dirname "$0")" && pwd)
ACCOUNT=$(gcloud config list account --format "value(core.account)" | cut -d '@' -f1)
STARTUP_SCRIPT='startup.sh'

function upload_cluster_config() {
    gsutil cp "$BASEDIR/cluster-config.yaml" "gs://prod-micro-cluster/init-configs/$ACCOUNT.yaml"
}

function generate_startup_script() {
    cat << EOE >$STARTUP_SCRIPT
#!/bin/bash

sudo -i -u mc zsh << EOF
mkdir -p ~/cluster
cd ~/cluster
mcctl init -d docker
gsutil cp gs://prod-micro-cluster/init-configs/$ACCOUNT.yaml ~/cluster/cluster-config.yaml
mcctl configure
mcctl cluster start
EOF
EOE
}

function start_instance() {
    gcloud beta compute instances create $VM_NAME \
        --zone=$ZONE \
        --source-machine-image=$SOURCE_MACHINE_IMAGE_NAME \
        --metadata-from-file=startup-script=$STARTUP_SCRIPT
}

function print_login_guide() {
    cat <<EOF
You can login with the following command when the instance is ready.

$ gcloud beta compute ssh --zone "$ZONE" "mc@$VM_NAME" --project "$PROJECT"
EOF
}

upload_cluster_config
generate_startup_script
start_instance
print_login_guide
