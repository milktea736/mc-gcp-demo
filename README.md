# mc-gcp-demo

此專案是 GCP 讀書會紀錄, 僅提供簡單 micro cluster 示範. 如需進一步調整 micro cluster 或是 VM 的資源請與 Ivan 討論唷.

## 環境設定

* 請先安裝 gcloud cli 並進行登入

## 使用

* 在 cluster-config.yaml 設定想要的資源
* 在 `start-instance.sh` 內編輯 `VM_NAME`
* 執行 `start-instance.sh` 會進行以下事項
  * 以您登入 gcloud 的帳號, 在 cloud storage 的 micro cluster bucket 中建立 `$ACCOUNT.yaml`
  * 以預設的資源啟動 VM instance
  * 將您的設定檔同步到 VM instance
  * 啟動 micro cluster
